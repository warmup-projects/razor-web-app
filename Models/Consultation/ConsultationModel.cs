namespace razor.Models.Consultation;

public class ConsultationModel
{
    public int Id { get; set; }
    public string? Doctor { get; set; }
    public string? Patient { get; set; }
}