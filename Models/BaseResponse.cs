using System.Text.Json.Serialization;

namespace razor.Models;

public class BaseResponse<T>
{
    // [JsonPropertyName("code")]
    public string Code { get; set; } = null!;
    // [JsonPropertyName("message")]
    public string Message { get; set; } = null!;
    // [JsonPropertyName("data")]
    public T Data { get; set; } = default!;
}