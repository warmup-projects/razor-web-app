using NonFactors.Mvc.Grid;

namespace razor.Extensions;

public class GridProcessorExtension<T> : IGridProcessor<T>
{
    public GridProcessorExtension(GridProcessorType processorType, ProcessingCallback callback)
    {
        ProcessorType = processorType;
        Callback = callback;
    }

    private ProcessingCallback Callback { get; }
    public GridProcessorType ProcessorType { get; set; }

    public delegate IQueryable<T> ProcessingCallback(IQueryable<T> items);
    
    public IQueryable<T> Process(IQueryable<T> items)
    {
        return Callback(items);
    }
}