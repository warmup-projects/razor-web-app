namespace razor.Extensions;

public static class HttpClientExtension
{
    public const string PrimaryClientPath = "HttpClient:Primary:Name";
    
    public static string GetPrimaryClient(IConfiguration configuration) => configuration.GetValue<string>(PrimaryClientPath)!;
}