namespace razor.Services;

public static class ServiceInitialization
{
    public static void ServiceInit(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddHttpClient(configuration.GetValue<string>("HttpClient:Primary:Name")!, httpClient =>
        {
            httpClient.BaseAddress = new Uri(configuration.GetValue<string>("HttpClient:Primary:BaseUrl")!);
        });
    }
    
    public static void DependencyInjection(this IServiceCollection services)
    {
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
    }
}