using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using razor.Extensions;
using razor.Models;
using razor.Models.Consultation;

namespace razor.Pages;

public class IndexGridModel : PageModel
{
    private readonly ILogger<IndexGridModel> _logger;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly IConfiguration _configuration;

    [BindProperty(Name = "Doctor-contains", SupportsGet = true)]
    public string? SearchDoctor { get; set; }
    [BindProperty(Name = "Patient-contains", SupportsGet = true)]
    public string? SearchPatient { get; set; }
    public GridProcessorExtension<ConsultationModel>.ProcessingCallback Callback { get; set; }
    public List<ConsultationModel> ConsultationList = new();

    public IndexGridModel(ILogger<IndexGridModel> logger, IHttpClientFactory httpClientFactory, IConfiguration configuration)
    {
        _logger = logger;
        _httpClientFactory = httpClientFactory;
        _configuration = configuration;
    }

    public async Task OnGet()
    {
        Callback = DataMutating;
        using HttpClient client = _httpClientFactory.CreateClient(HttpClientExtension.GetPrimaryClient(_configuration));
        var uriBuilder = new UriBuilder(client.BaseAddress + "consultation-list");
        uriBuilder.Query = String.Format("patient={0}&doctor={1}", SearchPatient, SearchDoctor);

        try
        {
            BaseResponse<List<ConsultationModel>>? consultations =
                await client.GetFromJsonAsync<BaseResponse<List<ConsultationModel>>>(uriBuilder.Uri);

            if (consultations != null)
            {
                ConsultationList = consultations.Data;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }

    private IQueryable<ConsultationModel> DataMutating(IQueryable<ConsultationModel> items)
    {
        List<ConsultationModel> tempItems = ConsultationList;
        return tempItems.Select(item =>
        {
            var consultationModelTask = DataMutating(item);
            consultationModelTask.Wait();

            if (consultationModelTask.IsCompletedSuccessfully)
            {
                var result = consultationModelTask.Result;
                result.Id = item.Id;
                return result;
            }
            return item;
        }).AsQueryable();
    }

    private async Task<ConsultationModel> DataMutating(ConsultationModel item)
    {
        using HttpClient client = _httpClientFactory.CreateClient(HttpClientExtension.GetPrimaryClient(_configuration));
        var uriBuilder = new UriBuilder(client.BaseAddress + "consultation-decode");
        uriBuilder.Query = String.Format("patient={0}&doctor={1}", item.Patient, item.Doctor);

        try
        {
                BaseResponse<ConsultationModel>? consultation =
                    await client.GetFromJsonAsync<BaseResponse<ConsultationModel>>(uriBuilder.Uri);

                if (consultation != null) return consultation.Data;
                throw new Exception("Failed to decode data");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}