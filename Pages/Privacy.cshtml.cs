﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using razor.Extensions;
using razor.Models;
using razor.Models.Consultation;

namespace razor.Pages;

public class PrivacyModel : PageModel
{
    private readonly ILogger<PrivacyModel> _logger;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly IConfiguration _configuration;

    public List<ConsultationModel> ConsultationList = new();

    public PrivacyModel(ILogger<PrivacyModel> logger, IHttpClientFactory httpClientFactory, IConfiguration configuration)
    {
        _logger = logger;
        _httpClientFactory = httpClientFactory;
        _configuration = configuration;
    }
    
    public async Task OnGet()
    {
        using HttpClient client = _httpClientFactory.CreateClient(HttpClientExtension.GetPrimaryClient(_configuration));

        try
        {
            BaseResponse<List<ConsultationModel>>? consultations =
                await client.GetFromJsonAsync<BaseResponse<List<ConsultationModel>>>("consultation-list");

            if (consultations != null) ConsultationList = consultations.Data;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
    }
}

